import tkinter as tk
defaultFont = "Fira\ Sans" #Change this line if that font doesn't work on your machine

def clickFunction():
    resultLabel.config(text=f"\nThe circumference of your circle is {int(radiusEntry.get()) * 6.28} units, and the area of your circle is {int(radiusEntry.get()) * int(radiusEntry.get()) * 3.14} square units")

window = tk.Tk()
window.title("Circle Calculator")
window.configure(background="#121212")
window.geometry("800x400")

title = tk.Label(window, text="\n\nPlease enter the radius of your circle\n\n", bg="#121212", fg="white", font=defaultFont+" 24 bold")
title.pack()
radiusEntry = tk.Entry(window, text="Radius", bg="#444444", fg="white", font=defaultFont)
radiusEntry.pack()
spacer = tk.Label(window, text='\n', bg="#121212")
spacer.pack()
submitButton = tk.Button(window, command=clickFunction, text="Submit", bg="#767676", fg="white", font=defaultFont)
submitButton.pack()
resultLabel = tk.Label(window, text="", bg="#121212", fg="white", font=defaultFont)
resultLabel.pack()

window.mainloop()